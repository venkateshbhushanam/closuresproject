
function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    var count=0
    
    let obj={
        increment:function(){
            count=count+1
            return count
        },
        decrement:function(){
            count=count-1
            return count
        }
        
    }
    return obj
}



module.exports=counterFactory