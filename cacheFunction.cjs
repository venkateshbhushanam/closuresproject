

function cacheFunction(cb) {
    
        var cache = {};
        if(typeof(cb)==='function'){
            function invokdeCb(...x) {
                // console.log(Object.getOwnPropertyNames(cache))
                if (cache.hasOwnProperty(x)) {
                    // console.log('from cache')
                    return cache[x];}
                cache[x] = cb(x);
                return cache[x];

        };

        return invokdeCb
    }

    else{
        return function(){}
    }

}

module.exports=cacheFunction