function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
   if(typeof(cb)=='function'){
    function invokeCb(...num){
        for(let iterations=0;iterations<n;iterations++){
            cb(num)
            }
            }
            return invokeCb
        }
    else{
        return null
    }
}

module.exports=limitFunctionCallCount